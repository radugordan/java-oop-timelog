package org.sci.finalproj;

import org.sci.finalproj.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DogeRulzApplication /* implements CommandLineRunner */ {
	@Autowired //injects it here
	private UserService userService;

	public static void main(String[] args) {
		SpringApplication.run(DogeRulzApplication.class, args);
	}

}

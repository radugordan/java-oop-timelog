package org.sci.finalproj.repo;

import org.sci.finalproj.model.Timelog;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TimelogRepo extends CrudRepository<Timelog, Long> {}


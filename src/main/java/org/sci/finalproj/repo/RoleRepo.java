package org.sci.finalproj.repo;

import org.sci.finalproj.model.Role;
import org.springframework.data.repository.CrudRepository;

public interface RoleRepo extends CrudRepository<Role, Long> {

}

package org.sci.finalproj.controller;

import org.sci.finalproj.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class UserController {
    @Autowired
    private org.sci.finalproj.service.UserService userService;


    @RequestMapping("/home")
    public String myIndexPage() {
        return "index";
    }

    @RequestMapping("/")
    public String myDefaultPage() {
        return this.myIndexPage();
    }


   @RequestMapping("/register")
    public String myRegisterPage(Model model) {
        User emptyUser = new User();
        model.addAttribute("user", emptyUser);
        return "register";
    }

    @RequestMapping(value = "/registerUser", method = RequestMethod.POST)
    public String registerUser(@ModelAttribute("user") User user, BindingResult errors, Model model) {
        boolean registerResult = userService.register(user);
        if (registerResult) {
            return "confirmRegistration";
        } else {
            return "error";
        }
    }

    @RequestMapping("/login")
    public String myLoginPage(Model model) {
        User emptyUser = new User();
        model.addAttribute("user", emptyUser);
        return "login";
    }

    @RequestMapping(value = "/loginUser", method = RequestMethod.POST)
    public ModelAndView confirmLogin(@ModelAttribute("user") User user, BindingResult errors, Model model)
    {
        // logic to process input data boolean
        boolean loginResult = userService.login(user);
         if (loginResult) {
             ModelAndView mv = new ModelAndView("redirect:/confirmationLogin");
             mv.addObject("user", user);
             return new ModelAndView("confirmationLogin");
         } else {
             return new ModelAndView("error");
         }
    }


//    @GetMapping({"/confirmationLogin"})
//    public String confirmationLogin(ModelAndView model) {
//        return "confirmationLogin";
//    }
}

package org.sci.finalproj.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.sql.Timestamp;


@Entity
public class Timelog {
    private Integer projectId;
    private Integer employeeId;
    private Timestamp startTime;
    private Timestamp endTime;
    private Integer activityType;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    public Timelog() {
    }

    public Integer getProjectId() {
        return projectId;
    }

    public void setProjectId(Integer projectId) {
        this.projectId = projectId;
    }

    public Integer getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Integer employeeId) {
        this.employeeId = employeeId;
    }

    public Timestamp getStartTime() {
        return startTime;
    }

    public void setStartTime(Timestamp startTime) {
        this.startTime = startTime;
    }

    public Timestamp getEndTime() {
        return endTime;
    }

    public void setEndTime(Timestamp endTime) {
        this.endTime = endTime;
    }

    public Integer getActivityType() {
        return activityType;
    }

    public void setActivityType(Integer activityType) {
        this.activityType = activityType;
    }

    public Timelog(Integer projectId, Integer employeeId, Timestamp startTime, Timestamp endTime, Integer activityType) {
        this.projectId = projectId;
        this.employeeId = employeeId;
        this.startTime = startTime;
        this.endTime = endTime;
        this.activityType = activityType;
    }

    
}
